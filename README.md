# Auto Reload
Simple application to instant reload of modified files in browser.

No browser plugins.

No file modifications.


## Usage
**Dependencies**

The only dependency is Java version 8 or later, use `java -version` to check java version, output should be similar to
```
openjdk version "1.8.0_171"
OpenJDK Runtime Environment (build 1.8.0_171-8u171-b11-0ubuntu0.16.04.1-b11)
OpenJDK 64-Bit Server VM (build 25.171-b11, mixed mode)
```

**Installation**

Download [executable jar](https://gitlab.com/l.labaj/auto-reload/uploads/7edc0f4230dd4bc0be354f0bc511614d/autoreload.jar)
and run it using
```
java -jar autoreload.jar
```
Now open your browser and type `http://localhost:8081`. Application serves files from current working directory
and supports browsing directories and opening HTML and text files. Navigate to open a text file in browser and
in text editor, saving changes in the file now automatically reloads the browser.

To change port number, pass it as the only argument, for example
```
java -jar autoreload.jar 1234
```

Flag `-h` or `--help` is used to print help information.

## Licenses
- Javalin - [javalin.io](https://javalin.io) under [Apache License 2.0](https://github.com/tipsy/javalin/blob/master/LICENSE)
- Thymeleaf - [thymeleaf.org](https://www.thymeleaf.org) under [Apache License 2.0](https://github.com/thymeleaf/thymeleaf/blob/3.0-master/LICENSE.txt)
- SLF4J - [slf4j.org](https://www.slf4j.org/) under [MIT license](https://www.slf4j.org/license.html)
