package com.me.autoreload;

import java.nio.file.Files;
import java.nio.file.Path;

public class DirectoryEntry {
    private Path relative;
    private Path absolute;
    private Path parent;

    public DirectoryEntry(Path file, Path base) {
        this.relative = base.relativize(file);
        this.absolute = base.resolve(file);
        this.parent = base.relativize(absolute.getParent());
    }

    public String getName() {
        return absolute.getFileName().toString();
    }

    public String getAbsolute() {
        return absolute.toString();
    }

    public String getRelative() {
        return relative.toString();
    }

    public Path getParent() {
        return parent;
    }

    public boolean isDirectory() {
        return Files.isDirectory(absolute);
    }

    public boolean isFile() {
        return Files.isRegularFile(absolute);
    }

}
