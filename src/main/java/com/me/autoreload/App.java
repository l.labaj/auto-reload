package com.me.autoreload;

import io.javalin.Context;
import io.javalin.Javalin;
import io.javalin.embeddedserver.jetty.websocket.WsSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URLDecoder;
import java.nio.file.*;
import java.util.*;

import static io.javalin.embeddedserver.Location.EXTERNAL;
import static java.nio.file.StandardWatchEventKinds.*;
import static java.util.stream.Collectors.toList;

public class App {
    private static final Logger log = LoggerFactory.getLogger(App.class);

    private final String PREFIX = "/live/";
    private final String baseDir;
    private final Path basePath;
    private List<WsSession> wsSessions = new LinkedList<>();


    public static void main(String[] args) throws Exception {
        int port = 8081;
        if (args.length == 1) {
            String arg = args[0];
            if ("--help".equalsIgnoreCase(arg) || "-h".equalsIgnoreCase(arg)) {
                System.out.println("AutoReload - instant reload of modified files in browser.");
                System.out.println("Start the application in directory to be watched for modifications, then open browser at http://localhost:[PORT]");
                System.out.println("Default port is 8081, can be changed via command line argument.");
                System.out.println();
                System.out.println("Usage: java -jar autoreload.jar [PORT]");
                System.out.println("\t-h, --help \t print this help");
                System.exit(0);
            } else {
                try {
                    port = Integer.parseInt(arg);
                } catch (NumberFormatException e) {
                    log.error("Error parsing port number: '{}'", arg);
                    System.exit(1);
                }
            }
        }
        new App(port);
    }

    private App(int port) throws IOException, InterruptedException {
        baseDir = System.getProperty("user.dir");
        basePath = Paths.get(baseDir);

        log.info("Using port '{}", port);
        log.info("Using directory '{}'", baseDir);

        Javalin.create()
                .disableStartupBanner()
                .get("/", ctx -> ctx.redirect(PREFIX))
                .enableStaticFiles("/static/")
                .enableStaticFiles(baseDir, EXTERNAL)
                .get(PREFIX + "*", this::handleRequest)
                .ws("/ws", ws -> ws.onConnect(session -> wsSessions.add(session)))
                .port(port)
                .start();

        WatchService watchService = FileSystems.getDefault().newWatchService();
        Files.walk(basePath)
                .filter(Files::isDirectory)
                .forEach(p -> register(p, watchService));

        log.info("File watching started, directory = '{}'", baseDir);
        WatchKey key;
        while ((key = watchService.take()) != null) {
            for (WatchEvent event : key.pollEvents()) {
                Path parent = (Path) key.watchable();
                Path watchedPath = parent.resolve((Path) event.context());

                log.debug("Event kind '{}' for file '{}' in '{}'", event.kind(), event.context(), parent);

                boolean isDir = Files.isDirectory(watchedPath);

                if (event.kind() == ENTRY_MODIFY && !isDir) {
                    wsSessions.removeIf(s -> !s.isOpen());
                    for (WsSession wsSession : wsSessions) {
                        wsSession.getRemote().sendString("yo");
                    }
                } else if (event.kind() == ENTRY_CREATE && isDir) {
                    register(watchedPath, watchService);
                }
            }
            key.reset();
        }
        log.info("Exit");
    }


    private void handleRequest(Context context) throws IOException {
        String decoded = URLDecoder.decode(context.path(), "UTF-8");
        Path file = Paths.get(baseDir, decoded.replaceFirst(PREFIX,""));
        log.debug("Got path '{}' -> '{}'", decoded, file);

        if (Files.notExists(file)) {
            handleNotExists(file, context);
        } else if (Files.isDirectory(file)) {
            handleDirectory(file, context);
        } else {
            handleFile(file, context);
        }
    }

    private void handleNotExists(Path path, Context context) {
        log.debug("Handling NON-EXISTING '{}'", path);
        context.html(path + " not found");
    }

    private void handleFile(Path file, Context context) {
        log.debug("Handling FILE '{}'", file);

        Map<String, Object> model = new HashMap<>();
        model.put("prefix", PREFIX);
        model.put("file", new DirectoryEntry(file, basePath));

        context.renderThymeleaf("/templates/file.html", model);
    }

    private void handleDirectory(Path dir, Context context) throws IOException {
        log.debug("Handling DIRECTORY '{}'", dir);

        Comparator<Path> cmp = Comparator.<Path, Boolean>comparing(Files::isDirectory)
                .reversed()
                .thenComparing(Path::getFileName);

        List<DirectoryEntry> files = Files.walk(dir, 1)
                .skip(1)
                .sorted(cmp)
                .map(p -> new DirectoryEntry(p, basePath))
                .collect(toList());

        Map<String, Object> model = new HashMap<>();
        model.put("prefix", PREFIX);
        model.put("current", new DirectoryEntry(dir, basePath));
        model.put("files", files);

        context.renderThymeleaf("/templates/directory.html", model);
    }

    private void register(Path path, WatchService watchService) {
        try {
            path.register(watchService, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
